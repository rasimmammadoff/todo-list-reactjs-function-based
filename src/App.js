import React, { useState } from "react"

//Functional Component - todo items
function Todo({ todo, index, deleteTodo, completed }) {
    return (
        <div
            index={index}
            style={{ textDecoration: todo.completed ? "line-through" : "" }}>
            {todo.text}
            <div className="buttons">
                <button onClick={() => deleteTodo(index)}>Delete</button>
                <button onClick={() => completed(index)}>Completed</button>
            </div>
        </div>
    )
}

//Functional Component - Form for adding new todo
function TodoForm({ addTodo }) {
    const [value, SetValue] = useState("")

    //Function - Handle the form result
    const handleSubmit = (e) => {
        e.preventDefault();
        if (value) {
            addTodo(value)
            SetValue("")
        }
    }

    return (
        <form onSubmit={handleSubmit}>
            <input type="text" value={value} onChange={e => SetValue(e.target.value)} />
            <button className="button-add" onClick={handleSubmit}>Add</button>
        </form>
    )
}

//Main app
function App() {
    //Default todos
    const [todos, SetTodos] = useState([
        { text: "Learn React", completed: false },
        { text: "Practice Django", completed: false },
        { text: "Learn Git", completed: false },
        { text: "Watch tutorials", completed: false },
    ])
    //Function - Add item to the todolist
    const addTodo = text => {
        const newtodo = {
            text: text,
            completed: false
        }
        const newTodos = [...todos, newtodo]
        SetTodos(newTodos)
    }
    //Function - Delete item from the todolist
    const deleteTodo = index => {
        const newTodos = todos.filter((item) => item !== todos[index])
        SetTodos(newTodos)
    }

    //Function - Line through the completed todo
    const completed = index => {
        const newTodos = todos.filter(function (item) {
            if (item === todos[index]) {
                item.completed = true
            }
            return item
        })
        SetTodos(newTodos)
    }

    return (
        <div>
            <h2 className="title">Todo List</h2>
            <div className="todo-list">
                {todos.map((todo, index) => (
                    <Todo key={index} index={index} todo={todo} deleteTodo={deleteTodo} completed={completed} />
                ))}
                <TodoForm addTodo={addTodo} />
            </div>
        </div>
    )
}

export default App